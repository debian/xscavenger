/*  SCAVENGER by David Ashley  dash@netcom.com dash5@geocities.com */

#ifndef NAMES_H
#define NAMES_H

/* next should already be defined with makefile */
#ifndef LIBNAME
#define LIBNAME "/usr/X11R6/lib/scavenger"
#endif

#ifndef ICONPATH
#define ICONPATH "/usr/X11R6/include/X11/pixmaps/xscavenger-icon.xpm"
#endif

#define LOCALDIRNAME ".scavenger"
#define RCNAME "scavrc"
#define LEVELSNAME "levels.scl"
#define PLAYERNAME "player.nam"
#define ENVHOME "HOME"
#define ENVUSER "USER"

#endif
